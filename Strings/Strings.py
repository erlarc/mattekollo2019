from random import seed, randint

#PROBLEM
TARGET = "hej erland och thomas!"
ALPHABET = "abcdefghijklmnopqrstuvwxyz !?."

#CONF
ITERATIONS = 100000
GEN_SIZE = 1000
MUT_PROB = 10

#DEBUG
DEBUG = True
NUM_OUTPUT = ITERATIONS / 10

#STATIC
seed(None)

def run():
 print("Start... mut prob is", MUT_PROB)
 generation = setupInitialGeneration()
 print("Initial generation done, len=", len(generation))
 for i in range(0, ITERATIONS):
  generation = nextGeneration(generation)
  printGenStat(i, generation)

  if findBest(generation) == TARGET:
   print("Best found, stopping")
   break
#  if i % NUM_OUTPUT == 0:
#   print("Done with generation", i)
 print(findBest(generation))

def setupInitialGeneration():
 result = []
 for i in range(0, GEN_SIZE):
  result.append(randomInd())
 return result

def randomInd():
 result = ""
 for s in TARGET:
  result += randomChar()
 return result

def randomChar():
 pos = randint(1,len(ALPHABET))
 return ALPHABET[pos-1]

def printGenStat(itr, gen):
 if not DEBUG:
  return
 sum = 0
 for index, ind in zip(range(GEN_SIZE), gen):
  sum += fitness(ind)
 print("avg fitness", sum/GEN_SIZE, "gen", itr)

# print("best fitness", fitness(gen[0]), gen[0])

def nextGeneration(oldGeneration):
 oldGeneration.sort(key=fitness)

# printGenStat(0, oldGeneration)

 newGeneration = []
 for i in range(0, GEN_SIZE, 2):
  dad = oldGeneration[i]
  mom = oldGeneration[i+1]
  child = mate(dad, mom)
  newGeneration.append(child)

 for index, ind in zip(range(GEN_SIZE-len(newGeneration)), oldGeneration):
  newGeneration.append(ind)
 return newGeneration

def mate(dad, mom):
 length = len(TARGET)
 dadpart = round(length / 2)
 result = ""
 for i in range(0, dadpart):
  result += dad[i]
 for i in range(dadpart, length):
  result += mom[i]

 result = maybeMutate(result)

 return result

def maybeMutate(ind):
 if randint(0, MUT_PROB) == 0:
  pos = randint(1, len(ind))
  newchar = randomChar()

  l = list(ind)
  l[pos-1] = newchar
  ind = "".join(l)
 return ind

def findBest(generation):
 return min(generation, key=fitness)

def fitness(ind):
 diff = 0
 for i in range(0, len(TARGET)):
  indV = ind[i]
  tarV = TARGET[i]
  diff += abs(ord(indV) - ord(tarV))
 return diff

run()
