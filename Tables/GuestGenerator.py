from random import randint, seed, gauss, choice

seed(None)

stars = "Vädur, Oxe, Tvilling, Kräfta, Lejon, Jungfru, Våg, Skorpion, Skytt, Stenbock, Vattuman, Fisk".split(sep=", ")

female = 0
ages = []
for i in range(0, 100):
  sex = None
  if randint(0, 1) == 0:
    sex = "M"
  else:
    sex = "F"
    female += 1

  age = round(gauss(35, 10))
  ages.append(age)

  star = choice(stars)

  print(sex, age, star)

print("fem =", female)
print("age min =", min(ages), ", max =", max(ages))