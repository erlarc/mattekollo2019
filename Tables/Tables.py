class Person:
 def __init__(age, starsign, sex):
  self.age = age
  self.starsign = starsign
  self.sex = sex

def cmprStarsign(a, b);
 if a == b:
  return 10
 if abs(a-b) == 1:
  return 5
 return 0

def cmprAge(a, b):
 return max(0, 10 - abs(a-b))

def cmprSex(a, b):
 if a == b:
  return 0
 return 5

def cmpr(a, b):
 return cmprStarsign(a.starsign, b.starsign) + cmprAge(a.age, b.age) + cmprSex(a.sex, b.sex)

def eval(seatinglist):
 sum = 0
 for index, person in enumerate(seatinglist):
  sum += cmpr(person, seatinglist[(i+1)%len(seatinglist)]
 return sum
