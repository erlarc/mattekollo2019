from random import seed, randint

ITERATIONS = 10000

MUT_PROB = 1000 #prob for mutation is 1/MUT_PROB
MUT_SIZE = 10 #max mut size in promille

POP_SIZE = 1000 #number of solutions in each generation
POP_SURVS = 100 #number of pops that surive to next generation
POP_PAIR = 500 #number of new pops to create by pairing two following sols

SOL_MAX = 100
SOL_MIN = 0
SOL_INITIAL_TICK = 100 #number of decimals for the initial generation

seed(None)

def lhs(x):
 return 10-x

def rhs(x):
 #r = 0
 #for i in range(1, 10):
 # r += 1 / (2**(i*x))
 #return r
 return x

def run():
 solutions = randomsolutions()
 for i in range(ITERATIONS):
  solutions = nextgeneration(solutions)
  generationdebug(i, solutions)
 solution = findbest(solutions)
 printresult(solution)

def nextgeneration(oldsols):
 oldsols.sort(key=fitness)
 newsols = []
 for i in range(0, POP_SURVS):
  newsols.append(oldsols[i])
 for i in range(0, 2*POP_PAIR, 2):
  newsols.append(mateandmutate(oldsols[i], oldsols[i+1]))
 for i in range(len(newsols), POP_SIZE):
  dad = oldsols[randint(0, POP_SIZE / 10)]
  mom = oldsols[randint(0, POP_SIZE / 10)]
  newsols.append(mateandmutate(mom, dad))
 return newsols

def fitness(x):
 return abs(lhs(x)-rhs(x))

def mateandmutate(d, m):
 return maybemutate(mate(d, m))

def mate(d, m):
 return (d+m)/2

def maybemutate(x):
 if randint(0, MUT_PROB) == 0:
  mut = randint(1, MUT_SIZE) / 1000
  if randint(0, 2) == 0:
   mut *= -1
  mut += 1
  return x * mut
 return x

def randomsolutions():
 solutions = []
 for i in range(0, POP_SIZE):
  solutions.append(randomsolution())
 return solutions

def randomsolution():
 # TODO there are probably better python default random methods for this
 i = randint(SOL_MIN*SOL_INITIAL_TICK, SOL_MAX*SOL_INITIAL_TICK)
 return i / SOL_INITIAL_TICK

def findbest(solutions):
 return min(solutions, key=fitness)

def verifyandprintconf():
 print("ITERATIONS =", ITERATIONS)
 assert ITERATIONS >= 1

 print("MUT_PROB =", MUT_PROB)
 assert MUT_PROB >= 1
 print("MUT_SIZE =", MUT_SIZE)
 assert MUT_SIZE >= 1

 print("POP_SIZE =", POP_SIZE)
 assert POP_SIZE > 0
 assert POP_SIZE % 10 == 0
 print("POP_SURVS =", POP_SURVS)
 assert POP_SURVS >= 0
 print("POP_PAIR =", POP_PAIR)
 assert POP_PAIR >= 0
 assert POP_PAIR*2 <= POP_SIZE

 print("SOL_MAX =", SOL_MAX)
 assert SOL_MAX > 0
 print("SOL_MIN =", SOL_MIN)
 assert SOL_MIN >= 0
 assert SOL_MAX > SOL_MIN
 print("SOL_INITIAL_TICK =", SOL_INITIAL_TICK)
 assert SOL_INITIAL_TICK >= 0
 assert SOL_INITIAL_TICK % 10 == 0

def generationdebug(i, genration):
 if i % (ITERATIONS/10) == 0:
  print("Done with generation", i)

def printresult(solution):
 print("Best result found is", solution, "with fitness =", fitness(solution))

verifyandprintconf()
run()
